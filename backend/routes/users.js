const express = require('express');
const { body } = require('express-validator');
const RolController = require('../controller/RolController');
const PersonaController = require('../controller/PersonaController');
const ProfesorController = require('../controller/ProfesorController');
const CuentaController = require('../controller/CuentaController');
const MateriaController = require('../controller/MateriaController');
const TareaController = require('../controller/TareaController');
const CursaController = require('../controller/CursaController');
  
const router = express.Router();
const rolController = new RolController();
const personaController = new PersonaController();
const profesorController = new ProfesorController();
const cuentaController = new CuentaController();
const materiaController = new MateriaController();
const tareaController = new TareaController();
const cursaController = new CursaController();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});

// Rutas relacionadas con roles
router.get('/roles', rolController.listar);

// Rutas relacionadas con personas
router.post('/persona/modificar', personaController.modificar);
router.post('/persona/guardar', [
  body('apellidos', 'Ingrese su apellido').exists(),
  body('nombres', 'Ingrese algún dato').exists()
], personaController.guardar);
router.get('/persona', personaController.listar);
router.get('/persona/obtener', personaController.obtener);

// Rutas relacionadas con profesores
router.post('/profesor/guardar', [
  body('apellidos', 'Ingrese su apellido').exists(),
  body('nombres', 'Ingrese algún dato').exists()
], profesorController.guardar);

// Rutas relacionadas con cuentas
router.post('/sesion', cuentaController.sesion);

// Rutas relacionadas con tareas
router.post('/tarea/guardar', tareaController.guardar);
router.get('/cursa/tareas', cursaController.extraerNombreTarea);
router.get('/practicas/listar', tareaController.listar);

// Rutas relacionadas con cursos
router.post('/cursa/guardar', cursaController.guardar);
router.get('/cursa/external', cursaController.extraerIdCursa);
router.get('/cursa/listar', cursaController.extraerTodo);

module.exports = router;
