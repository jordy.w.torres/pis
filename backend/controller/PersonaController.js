'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var persona = models.persona;
var rol = models.rol;
var rol_persona = models.rol_persona;
var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const salRounds = 8;
class PersonaController {
    async listar(req, res) {
        var listar = await estudiante.findAll({
            attributes: ['apellidos', 'nombres', 'external_id', 'direccion', 'identificacion', 'tipo_identificacion']
        });
        res.status(200);
        res.json({ msg: "0k", code: 200, info: listar });

    }
    async listar(req, res) {
        var listar = await materia.findAll({
            attributes: ['apellidos', 'nombres', 'external_id', 'direccion', 'identificacion', 'tipo_identificacion']
        });
        res.status(200);
        res.json({ msg: "0k", code: 200, info: listar });

    }
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await estudiante.findOne({
            where: { external_id: external }, include: { model: models.cuenta, as: "cuenta", attributes: ['email'] },
            attributes: ['apellidos', 'external_id', 'nombres', 'direccion', 'identificacion', 'tipo_identificacion', 'edad','telefono']
        });
        if (lista == null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var rol_id = req.body.external_rol;
    
            if (1 === 1) {
                let rolAux = await rol.findOne({ where: { external_id: rol_id } });
                console.log(rolAux);
    
                if (rolAux) {
                    var claveHash = function (clave) {
                        return bcrypt.hashSync(clave, bcrypt.genSaltSync(salRounds), null);
                    };
    
                    // Crear la instancia de persona y guardarla en la base de datos
                    const nuevaPersona = await persona.create({
                        identificacion: req.body.identificacion,
                        tipo_identificacion: req.body.tipo_identificacion,
                        nombres: req.body.nombres,
                        edad: req.body.edad,
                        apellidos: req.body.apellidos,
                        direccion: req.body.direccion,
                        telefono: req.body.telefono,
                    });
    
                    // Utilizar el id generado para asignarlo al campo id_persona en el objeto data
                    var data = {
                        rol_persona: {
                            id_rol: rolAux.id,
                            id_persona: nuevaPersona.id,
                        },
                        cuenta: {
                            email: req.body.email,
                            clave: claveHash(req.body.clave),
                            id_persona: null, // Inicialmente asignamos un valor null
                        },
                    };
                    
                    // Crear la instancia de rol_persona y guardarla en la base de datos
                    const nuevaRolPersona = await rol_persona.create(data.rol_persona);
                    data.cuenta.id_persona = nuevaRolPersona.id; // Asignar el id_rol_persona correspondiente a la cuenta
                    
                    // Ahora puedes crear la instancia de cuenta con los datos adecuados
                    const nuevaCuenta = await cuenta.create(data.cuenta);
                    // Resto del código para manejar la respuesta...
    
                    // Por ejemplo, enviar una respuesta de éxito al cliente
                    return res.status(200).json({ message: 'Registro exitoso.',code: 200});
                } else {
                    // Manejo para cuando no se encuentra el rolAux
                    return res.status(404).json({ message: 'No se encontró el rol proporcionado.', code:400 });
                }
            } else {
                // Otro manejo si la condición no se cumple
                return res.status(400).json({ message: 'Condición no cumplida.' });
            }
        } else {
            // Manejo para errores en la validación
            return res.status(422).json({ errors: errors.array() });
        }
    }

    async modificar(req, res) {
        var person = await estudiante.finOne({ where: { external_id: req.body.external } });
        if (estudiante === null) {
            res.json({ msg: "No existe el registro", code: 400 });
        } else {
            person.identificacion = req.body.identificacion;
            person.tipo_identificacion = req.dody.tipo_identificacion;
            person.apellidos = req.body.apellidos;
            person.nombres = req.body.nombre;
            person.direccion = req.body.direccion;
            person.external_id = uuid.v4();
            var result = await person.save();
            if (result === null) {

            } else {
                res.status(200);
                res.json({ msg: "se han modificado sus datos", code: 200 });
            }
        }
    }
}
module.exports = PersonaController;