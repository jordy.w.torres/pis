'use strict';

module.exports = (sequelize,DataTypes) => {
  const practica = sequelize.define('practica', {
    tema: { type: DataTypes.STRING(50),allowNull: false,},
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    descripcion: {type: DataTypes.STRING(300),allowNull: false, },
    fecha_envio: { type: DataTypes.DATE,allowNull: false,},
    fecha_entrega: { type: DataTypes.DATE,allowNull: false, },
    estado: {type: DataTypes.BOOLEAN, allowNull: false,},
    archivo: {type: DataTypes.STRING,allowNull: false, }
  }, {
    freezeTableName: true
  });

  practica.associate = function(models) {
    practica.belongsTo(models.cursa, { foreignKey: 'id_cursa', as: 'cursa' });
    
  };

  return practica;
};
