'use strict';
const { DataTypes } = require('sequelize');
module.exports = (sequelize,DataTypes) => {
  const cursa = sequelize.define('cursa', {
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    nota: {type: DataTypes.DOUBLE,allowNull:true}
  }, { freezeTableName: true });

  cursa.associate = function (models) {
    cursa.belongsTo(models.materia, { foreignKey: 'id_materia', as: 'materia' });
    cursa.belongsTo(models.matricula, { foreignKey: 'id_matricula', as: 'matricula' });
    cursa.belongsTo(models.rol_persona, { foreignKey: 'id_profesor', as: 'profesor' });
  };
  return cursa;
};
