'use strict';
const { UUIDV4 } = require("sequelize");
const { DataTypes } = require('sequelize');
module.exports = (sequalize, DataTypes) => {
    const persona = sequalize.define('persona', {
        nombres: {type: DataTypes.STRING(50), defaultValue: "Sin_Datos"},
        apellidos: {type: DataTypes.STRING(50), defaultValue: "Sin_Datos"},
        identificacion: { type: DataTypes.STRING(20), unique: true,allowNull: false, defaultValue: "Sin_Datos" },
        tipo_identificacion: {type: DataTypes.ENUM("CEDULA","PASAPORTE","RUC"), allowNull: false, defaultValue: "CEDULA" },
        direccion: { type: DataTypes.STRING(100),allowNull: true, defaultValue: "Sin_Datos"},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true}, 
        edad: {type: DataTypes.INTEGER(3),allowNull: false},
    }, {freezeTableName: true});

    persona.associate = function(models) {
        persona.hasOne(models.cuenta, { foreignKey: 'id_persona', as: 'cuenta' });
        persona.hasOne(models.rol_persona, { foreignKey: 'id_persona', as: 'rol_persona' }); 
    }

    return persona;
}