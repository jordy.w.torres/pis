'use strict';
const { DataTypes } = require('sequelize');

module.exports = (sequelize,DataTypes) => {
  const materia = sequelize.define('materia', {
    nombre: {type: DataTypes.STRING(50), allowNull: false },
    unidades: { type: DataTypes.INTEGER,allowNull: false },
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
  }, {
    freezeTableName: true
  });
  materia.associate = function (models) {
    materia.hasMany(models.cursa, { foreignKey: 'id_materia', as: 'cursa' });
    materia.hasMany(models.ciclo, { foreignKey: 'id_materia', as: 'ciclo' });
  };
  return materia;
};
