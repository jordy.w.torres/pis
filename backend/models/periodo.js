'use strict';
const { UUIDV4 } = require("sequelize");
const { DataTypes } = require('sequelize');
module.exports = (sequelize,DataTypes) => {
    const periodo = sequelize.define('periodo', {
        mesComienza: { type: DataTypes.DATE, allowNull: false },
        mesFin: { type: DataTypes.DATE, allowNull: false },
        anioFIn: { type: DataTypes.DATE, allowNull: false },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    }, {
        freezeTableName: true
    });
    periodo.associate = function (models){
        periodo.hasMany(models.matricula, {foreignKey: 'id_periodo'});
    }
    return periodo;
}; 