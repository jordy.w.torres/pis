'use strict';
const { DataTypes } = require('sequelize');

module.exports = (sequelize,DataTypes) => {
  const entrega = sequelize.define('entrega', {
    tema: {type: DataTypes.STRING(50),allowNull: false,},
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    descripcion: {type: DataTypes.STRING(300),allowNull: false, },
    fecha_envio: { type: DataTypes.DATE,allowNull: false,},
    fecha_entrega: { type: DataTypes.DATE,allowNull: false, },
    estado: {type: DataTypes.BOOLEAN, allowNull: false,},
    archivo: {type: DataTypes.STRING,allowNull: false, }
  }, {
    freezeTableName: true
  });

  entrega.associate = function(models) {
    entrega.belongsTo(models.practica, { foreignKey: 'id_practica' });
  };

  return entrega;
};
