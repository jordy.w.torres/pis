'use strict';
const { UUIDV4 } = require("sequelize");
const { DataTypes } = require('sequelize');
module.exports = (sequelize,DataTypes) => {
  const rol_persona = sequelize.define('rol_persona', {
    external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4}
  }, { freezeTableName: true });
  rol_persona.associate = function(models) {
    rol_persona.belongsTo(models.persona, {foreignKey: 'id_persona',as:'persona'});
    rol_persona.belongsTo(models.rol, {foreignKey: 'id_rol',as:'rol'});
  }

  return rol_persona;
};
