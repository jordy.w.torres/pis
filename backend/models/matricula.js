'use strict';
const { DataTypes } = require('sequelize');
module.exports = (sequelize,DataTypes) => {
  const matricula = sequelize.define('matricula', {
    //id_materia: {type: DataTypes.INTEGER, allowNull: false},
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    estado: {type: DataTypes.BOOLEAN,defaultValue: true 
  }},{
    freezeTableName: true
  });

  matricula.associate = function(models) {
    matricula.belongsTo(models.periodo, {foreignKey: 'id_periodo'});
    matricula.belongsTo(models.rol_persona, { foreignKey: 'id_rol_persona', as: 'persona'});
  };

  return matricula;
};
 