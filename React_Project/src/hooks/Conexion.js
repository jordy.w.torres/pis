const URL = "http://localhost:3006/api"
export const InicioSesion = async (data) => {
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json",
          
    };
    const datos = await (await fetch(URL + "/sesion", {
        method: "POST",
        headers:headers,
        body: JSON.stringify(data)
    })).json();
    return datos;
}
export const obtenerMateria = async () => {
    try {
      const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
      const respuesta = await fetch(URL + "/cursa/materias", {
        method: "GET",
        headers: cabeceras
      });
  
      if (!respuesta.ok) {
        throw new Error("Error al obtener los datos de autos");
      }
  
      const datos = await respuesta.json();
      return datos.info; // Solo devolvemos la propiedad "info" de la respuesta
  
    } catch (error) {
      console.error(error);
      // Manejar el error de alguna manera adecuada en tu aplicación
    }
  };
  export const obtenerExternal = async () => {
    try {
      const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
      const respuesta = await fetch(URL + "/cursa/external", {
        method: "GET",
        headers: cabeceras
      });
  
      if (!respuesta.ok) {
        throw new Error("Error al obtener los datos de autos");
      }
  
      const datos = await respuesta.json();
      return datos.info; // Solo devolvemos la propiedad "info" de la respuesta
  
    } catch (error) {
      console.error(error);
      // Manejar el error de alguna manera adecuada en tu aplicación
    }
  };
  export const obtenerPractica = async () => {
    try {
      const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
      const respuesta = await fetch(URL + "/practicas/listar", {
        method: "GET",
        headers: cabeceras
      });
  
      if (!respuesta.ok) {
        throw new Error("Error al obtener los datos de autos");
      }
  
      const datos = await respuesta.json();
      return datos.info; // Solo devolvemos la propiedad "info" de la respuesta
  
    } catch (error) {
      console.error(error);
      // Manejar el error de alguna manera adecuada en tu aplicación
    }
  };
  export const obtenerTodo = async () => {
    try {
      const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
      const respuesta = await fetch(URL + "/cursa/listar", {
        method: "GET",
        headers: cabeceras
      });
  
      if (!respuesta.ok) {
        throw new Error("Error al obtener los datos de autos");
      }
  
      const datos = await respuesta.json();
      return datos.info; // Solo devolvemos la propiedad "info" de la respuesta
  
    } catch (error) {
      console.error(error);
      // Manejar el error de alguna manera adecuada en tu aplicación
    }
  };
  export const obtenerListaTareas = async () => {
    try {
      const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
      const respuesta = await fetch(URL + "/cursa/tareas", {
        method: "GET",
        headers: cabeceras
      });
  
      if (!respuesta.ok) {
        throw new Error("Error al obtener los datos de autos");
      }
  
      const datos = await respuesta.json();
      return datos.info; // Solo devolvemos la propiedad "info" de la respuesta
  
    } catch (error) {
      console.error(error);
      // Manejar el error de alguna manera adecuada en tu aplicación
    }
  };
  export const GuardarPractica = async (data) => {
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json",
          
    };
    const datos = await (await fetch(URL + "/tarea/guardar", {
        method: "POST",
        headers:headers,
        body: JSON.stringify(data)
    })).json();
    return datos;
}

