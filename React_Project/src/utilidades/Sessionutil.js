export const saveToken = (token) => {
    localStorage.setItem('token', token);
}
export const getToken = () => {
    return localStorage.getItem('token');
}
export const borrarSession = () => {
    localStorage.clear();
}
export const estaSesion = () => {
    let token = localStorage.getItem('token');
    return (token && (token !== 'undefine'|| token != null|| token != 'null'));
}