import React, { useEffect, useState } from 'react';
import { obtenerPractica } from '../hooks/Conexion';
import { borrarSession } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensaje';
import '../css/ListaTareasPendientes.css'
import 'bootstrap/dist/css/bootstrap.css';
import DataTable from 'react-data-table-component';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';
function ListaTareasPendientes() {
  const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
  const navigate = useNavigate();
  const [data, setDatos] = useState([]);

  const handleListaTareas = () => {
    navigate("/AgregarPractica");
  }

    useEffect(() => {
      obtenerPractica()
        .then((info) => {
          if (info.error === true && info.mensaje === 'Acceso denegado. Token a expirado') {
            borrarSession();
            mensajes(info.mensaje);
            navigate("/Login");
          } else {
            setDatos(info);
            console.log(info);
          }
        })
        .catch((error) => {
          console.error(error);
          // Manejar el error de alguna manera adecuada en tu aplicación
        });
    }, []);
  


  const columns = [
    {
      name: 'TEMA',
      selector: (row) => row.tema,
    },
      {
        name: 'DESCRIPCIÓN',
        selector: (row) => row.descripcion,
      },
    {
      name: 'Acciones',
       //onClick={() => handleModificar(row)}
      cell: (row) => (
        <button className="btn btn-primary" onClick={handleListaTareas}>
          Agregar Practica
        </button>
      ),
    },
    //  <ModificarAuto onModificarAuto={handleModificarAuto} />--->
  ];
  return (
    
      <>
    
      <DataTable
      columns={columns}
      data={data}
      expandableRows
      expandableRowsComponent={ExpandedComponent}
      className='Tabla'
      
     />
     
  <div className="model_box">
      <Modal
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
        </Modal.Header>
       
        <Modal.Body>
              
        </Modal.Body>
      </Modal>
    </div>
  </>
  );
}

export default ListaTareasPendientes;
