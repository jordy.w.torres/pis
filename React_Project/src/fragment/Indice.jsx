import React from 'react';
import { useNavigate } from 'react-router';
import '../css/in.css';
import ListaTareasPendientes from './ListaTareasPendientes';

const Indice = () => {
  const navigation = useNavigate();
  const handleCursos = () => {
    navigation('/ListaCursos');
  };

  const handlesalir = () => {
    navigation('/');
  };

  const TareasCreadas = () => {
    navigation('/login');
  };

  return (
    <div className="container-fluid">
     <nav className="navbar navbar-dark bg-dark">
        <ul className="navbar-nav d-flex">
          <li className="nav-item">
            <a className="nav-link" href="#">Inicio</a>
          </li>
          <li className="nav-item">
          <a className="nav-link" href="#">Servicios</a>
            
          </li>
          <li className="nav-item">
          <a className="nav-link" onClick={handleCursos}>Cursos</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#">Contacto</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#">Salir</a>
          </li>
        </ul>
      </nav>

      <div className="row">
        <div className="col-2 bg-dark">
          {/* ... */}
          <ul className="nav nav-pills flex-column mt-4">
            <li className="nav-item">
              <a href="" className="nav-link active" aria-current="page">Algoritmos y Estructuras de Datos</a>
            </li>
            <li className="nav-item">
              <a href="" className="nav-link">Bases de Datos</a>
            </li>
            <li className="nav-item">
              <a href="" className="nav-link">Programación Orientada a Objetos</a>
            </li>
            <li className="nav-item">
              <a href="" className="nav-link">Ingeniería de Software</a>
            </li>
          </ul>
        </div>
        <div className="col">
          <ListaTareasPendientes />
        </div>
      </div>
    </div>
  );
};

export default Indice;
