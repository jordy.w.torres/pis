import { Routes, Route, useLocation, Navigate } from 'react-router-dom';
import AgregarPractica from './fragment/AgregarPracticaEstudiante';
import AgregarPracticaProfesor from './fragment/AgregarPracticaProfesor';
import  CursosCompletados from './fragment/Cursoscompletados';
import Indice from './fragment/Indice';
import Login from './fragment/Login';
import ListaCursos from './fragment/ListaCursos';
import TareasCreadas from './fragment/TareasCreadas';
import CrearTareas from './fragment/CrearTareas';

import ListaTareasPendientes from './fragment/ListaTareasPendientes';

import { estaSesion } from './utilidades/Sessionutil';
import IndiceProfesor from './fragment/IndiceProfesor';
function App() {

  const Middeware = ({children}) => {
    const autenticado = estaSesion();
    const location = useLocation();
    if (autenticado) {
      return children;
    } else {
      return <Navigate to= '/sesion' state={location}/>;
    }
  }
  const MiddewareSesion = ({children}) => {
    const autenticado = estaSesion();
    const location = useLocation();
    if (autenticado) {
      return <Navigate to= '/inicio' />;
    } else {
      return children;
    }
  }
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<Login />}/>
        <Route path='/Indice' element={<Indice />} />
        <Route path='/ListaCursos' element={<ListaCursos/>} />
        <Route path='/TareasCreadas/:external' element={<TareasCreadas/>} />
        <Route path='/CrearTareas/:external' element={<CrearTareas/>} />
        <Route path='/AgregarPractica' element={<AgregarPractica/>} />
        <Route path='/ListaTareasPendientes' element={<ListaTareasPendientes />} />
        <Route path='/CursosCompletados' element={<CursosCompletados/>} />
        <Route path='/IndiceProfesor' element={<IndiceProfesor />} />
        <Route path='/TareasCreadas/:external' element={<AgregarPracticaProfesor />} />
      </Routes>
    </div>
  );
}
export default App;